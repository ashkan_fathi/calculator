import { createApp } from 'vue'
import App from './App.vue'
import './assets/styles/main.css'
import './registerServiceWorker'

createApp(App).mount('#app')
